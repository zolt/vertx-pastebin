FROM library/cassandra:latest

RUN mkdir -p /usr/local/pastebin
WORKDIR /usr/local/pastebin
COPY build/distributions/pastebin-0.1.tar /usr/local/pastebin

RUN tar -xvf pastebin-0.1.tar --strip-components=1 \
        && rm pastebin-0.1.tar

COPY pb.sh /usr/local/pastebin/pb.sh
EXPOSE 8080
ENTRYPOINT ["/usr/local/pastebin/pb.sh"]
CMD ["run"]