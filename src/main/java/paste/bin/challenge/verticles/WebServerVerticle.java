package paste.bin.challenge.verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.sockjs.BridgeEventType;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.PermittedOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import org.springframework.context.ApplicationContext;
import paste.bin.challenge.handlers.DeleteEntryHandler;
import paste.bin.challenge.handlers.GetEntriesHandler;
import paste.bin.challenge.handlers.PostEntryHandler;
import paste.bin.challenge.handlers.PutEntryHandler;

/**
 * Created with IntelliJ IDEA.<br/>
 * User: Timochkinea<br/>
 * Date: 25.09.16<br/>
 * Time: 16:06<br/>
 */
public class WebServerVerticle extends AbstractVerticle {

    private static final int PORT = 8080;

    private ApplicationContext applicationContext;

    public WebServerVerticle(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        super.start();
        Router router = Router.router(vertx);

        router.route().handler(BodyHandler.create());
        router.get("/entries").handler(applicationContext.getBean(GetEntriesHandler.class));
        router.post("/entries").handler(applicationContext.getBean(PostEntryHandler.class));
        router.put("/entries/:id").handler(applicationContext.getBean(PutEntryHandler.class));
        router.delete("/entries/:id").handler(applicationContext.getBean(DeleteEntryHandler.class));


        SockJSHandler sockJSHandler = SockJSHandler.create(vertx);
        BridgeOptions options = new BridgeOptions().addOutboundPermitted(new PermittedOptions().setAddress("entry.created"));

        router.route("/latest/*").handler(sockJSHandler.bridge(options, event -> {
            if(event.type() == BridgeEventType.SOCKET_CREATED) {
                System.out.println("A socket was created!");
            }
            event.complete(true);
        }));

        vertx.createHttpServer()
                .requestHandler(router::accept)
                .listen(PORT, handler -> {
                    if(handler.failed()) {
                        System.err.println("fail: " + handler.cause().getMessage());
                        startFuture.fail(handler.cause());
                    } else {
                        System.out.println("Fake pastebin started on http://localhost:8080/");
                        startFuture.complete();
                    }
                });
    }
}
