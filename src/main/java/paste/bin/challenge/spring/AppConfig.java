package paste.bin.challenge.spring;

import com.datastax.driver.core.Cluster;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

/**
 * Created with IntelliJ IDEA.<br/>
 * User: Timochkinea<br/>
 * Date: 25.09.16<br/>
 * Time: 21:34<br/>
 */
@Configuration
@ComponentScan(basePackages = {"paste.bin.challenge"})
@PropertySource("classpath:application.properties")
public class AppConfig {

    @Autowired
    private Environment env;

    @Bean
    public Cluster cluster() {
        return Cluster.builder()
                .withClusterName(env.getProperty("cluster.name"))
                .addContactPoint(env.getProperty("cassandra.contact.point"))
                .withPort(Integer.valueOf(env.getProperty("cassandra.contact.point.port")))
                .build();
    }
}
