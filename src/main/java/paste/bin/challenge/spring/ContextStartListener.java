package paste.bin.challenge.spring;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.<br/>
 * User: TimochkinEA<br/>
 * Date: 11.10.16<br/>
 * Time: 2:08<br/>
 * Create keyspace if not exist on application start
 */
@Component
public class ContextStartListener implements ApplicationListener {

    @Autowired
    private Cluster cluster;

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if(!(event instanceof ContextRefreshedEvent) ||
                ((ApplicationContextEvent)event).getApplicationContext().getParent() != null) {
            return;
        }

        try(Session session = cluster.newSession()) {
            session.execute("CREATE KEYSPACE IF NOT EXISTS pastebin WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '3'}  AND durable_writes = true");
            session.execute("CREATE TABLE IF NOT EXISTS pastebin.paste_entry (\n" +
                    "    id uuid,\n" +
                    "    added timestamp,\n" +
                    "    body text,\n" +
                    "    expires timestamp,\n" +
                    "    is_private boolean,\n" +
                    "    secret uuid,\n" +
                    "    title text,\n" +
                    "    PRIMARY KEY (id, added)\n" +
                    ") WITH CLUSTERING ORDER BY (added DESC)");
        }
    }
}
