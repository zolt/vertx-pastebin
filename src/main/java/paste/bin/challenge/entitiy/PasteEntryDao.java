package paste.bin.challenge.entitiy;

import com.datastax.driver.mapping.Result;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Query;

import java.util.UUID;

/**
 * Created with IntelliJ IDEA.<br/>
 * User: TimochkinEA<br/>
 * Date: 03.10.16<br/>
 * Time: 0:07<br/>
 */
@Accessor
public interface PasteEntryDao {

    /**
     * All public entries
     */
    @Query("select id, title, body, added, expires from pastebin.paste_entry where is_private = false ALLOW FILTERING")
    Result<PasteEntry> publicEntries();

    /**
     * Entry for delete/update
     * @param id        {@link PasteEntry#id}
     * @param secret    {@link PasteEntry#secret}
     */
    @Query("select * from pastebin.paste_entry where id = :id and secret = :secret ALLOW FILTERING")
    Result<PasteEntry> byIdAndSecretKey(UUID id, UUID secret);
}
