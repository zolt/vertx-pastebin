package paste.bin.challenge.entitiy;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.<br/>
 * User: TimochkinEA<br/>
 * Date: 02.10.16<br/>
 * Time: 21:54<br/>
 */
@Table(keyspace = "pastebin", name = "paste_entry")
@NoArgsConstructor
@Getter
@Setter
public class PasteEntry {

    @PartitionKey
    @Column(name = "id")
    private UUID id;

    @Column(name = "title")
    private String title;

    @Column(name = "body")
    private String body;

    @Column(name = "added")
    private Date added;

    @Column(name = "expires")
    private Date expires;

    @JsonProperty("private")
    @Column(name = "is_private")
    private boolean isPrivate;

    @Column(name = "secret")
    private UUID secret;

    public boolean getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }
}
