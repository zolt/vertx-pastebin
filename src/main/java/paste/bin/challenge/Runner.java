package paste.bin.challenge;

import io.vertx.core.*;
import org.springframework.context.*;
import org.springframework.context.annotation.*;
import paste.bin.challenge.spring.*;
import paste.bin.challenge.verticles.*;

/**
 * Created with IntelliJ IDEA.<br/>
 * User: Timochkinea<br/>
 * Date: 24.09.16<br/>
 * Time: 18:04<br/>
 * Application Runner
 */
public class Runner {

    /** */
    public static void main(String[] args) {
        final Vertx vertx = Vertx.vertx();
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        vertx.deployVerticle(new WebServerVerticle(context));
    }
}
