package paste.bin.challenge.util;


import io.vertx.core.json.JsonObject;
import org.springframework.stereotype.Component;
import paste.bin.challenge.entitiy.PasteEntry;

import java.time.Instant;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.<br/>
 * User: TimochkinEA<br/>
 * Date: 03.10.16<br/>
 * Time: 21:52<br/>
 * Utility component for mapping json data to entity
 */
@Component
public class DataMapper {

    /**
     * Map to {@link PasteEntry}
     * @param data      source json
     * @param entry     target entry
     */
    public void fromJson(JsonObject data, PasteEntry entry) {
        entry.setBody(data.getString("body") == null ? "null" : data.getString("body"));
        entry.setTitle(data.getString("title") == null ? "null" : data.getString("title"));
        Instant inst = data.getInstant("expires");
        if(inst != null) {
            entry.setExpires(Date.from(inst));
        } else {
            entry.setExpires(new Date(Long.MAX_VALUE));
        }
        entry.setIsPrivate(data.getBoolean("private") == null ? false : data.getBoolean("private"));
    }
}
