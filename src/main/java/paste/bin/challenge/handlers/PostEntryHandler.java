package paste.bin.challenge.handlers;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import paste.bin.challenge.entitiy.PasteEntry;
import paste.bin.challenge.util.DataMapper;

import java.util.Date;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.<br/>
 * User: TimochkinEA<br/>
 * Date: 02.10.16<br/>
 * Time: 18:00<br/>
 * Post entry and return secret key
 */
@Component
public class PostEntryHandler implements Handler<RoutingContext> {

    private Cluster cluster;

    private DataMapper dataMapper;

    @Autowired
    public PostEntryHandler(Cluster cluster, DataMapper dataMapper) {
        this.cluster = cluster;
        this.dataMapper = dataMapper;
    }

    @Override
    public void handle(RoutingContext event) {


        try(Session session = cluster.newSession()) {
            MappingManager manager = new MappingManager(session);
            Mapper<PasteEntry> mapper = manager.mapper(PasteEntry.class);

            PasteEntry entry = new PasteEntry();
            entry.setId(UUID.randomUUID());
            JsonObject data = event.getBodyAsJson();
            dataMapper.fromJson(data, entry);
            entry.setAdded(new Date());
            entry.setSecret(UUID.randomUUID());

            mapper.save(entry);

            EventBus eventBus = event.vertx().eventBus();
            eventBus.publish("entry.created", Json.encodePrettily(entry));
            event.response().end(new JsonObject().put("secret", entry.getSecret().toString()).encodePrettily());
        }
    }
}
