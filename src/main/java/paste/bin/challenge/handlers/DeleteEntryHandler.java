package paste.bin.challenge.handlers;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.MappingManager;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import paste.bin.challenge.entitiy.PasteEntry;
import paste.bin.challenge.entitiy.PasteEntryDao;

import java.util.UUID;

/**
 * Created with IntelliJ IDEA.<br/>
 * User: TimochkinEA<br/>
 * Date: 02.10.16<br/>
 * Time: 18:01<br/>
 */
@Component
public class DeleteEntryHandler implements Handler<RoutingContext> {

    private Cluster cluster;

    @Autowired
    public DeleteEntryHandler(Cluster cluster) {
        this.cluster = cluster;
    }

    @Override
    public void handle(RoutingContext event) {
        JsonObject data = event.getBodyAsJson();
        if(data == null || !data.containsKey("secret")) {
            event.response().setStatusCode(HttpResponseStatus.BAD_REQUEST.code()).end(new JsonObject().put("message", "secret key is required").encodePrettily());
        } else {
            try(Session session = cluster.newSession()) {
                UUID id = UUID.fromString(event.request().getParam("id"));
                MappingManager manager = new MappingManager(session);
                PasteEntryDao accessor = manager.createAccessor(PasteEntryDao.class);
                UUID secret = UUID.fromString(data.getString("secret"));
                PasteEntry entry = accessor.byIdAndSecretKey(id, secret).one();
                manager.mapper(PasteEntry.class).delete(entry);
                event.response().end(new JsonObject().put("message", "success").encodePrettily());
            }
        }
    }
}
