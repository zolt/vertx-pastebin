package paste.bin.challenge.handlers;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Handler;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import paste.bin.challenge.entitiy.PasteEntry;
import paste.bin.challenge.entitiy.PasteEntryDao;
import paste.bin.challenge.util.DataMapper;

import java.util.UUID;

/**
 * Created with IntelliJ IDEA.<br/>
 * User: TimochkinEA<br/>
 * Date: 02.10.16<br/>
 * Time: 18:00<br/>
 */
@Component
public class PutEntryHandler implements Handler<RoutingContext> {

    private Cluster cluster;

    private DataMapper dataMapper;


    @Autowired
    public PutEntryHandler(Cluster cluster, DataMapper dataMapper) {
        this.cluster = cluster;
        this.dataMapper = dataMapper;
    }

    @Override
    public void handle(RoutingContext event) {

        JsonObject data = event.getBodyAsJson();
        if(data == null) {
            event.response().setStatusCode(HttpResponseStatus.BAD_REQUEST.code()).end(new JsonObject().put("message", "No data found!").encodePrettily());
            return;
        } else if (!data.containsKey("secret")) {
            event.response().setStatusCode(HttpResponseStatus.FORBIDDEN.code()).end(new JsonObject().put("message", "Secret key not found!").encodePrettily());
            return;
        }


        try(Session session = cluster.newSession()) {
            MappingManager manager = new MappingManager(session);
            PasteEntryDao accessor = manager.createAccessor(PasteEntryDao.class);

            UUID entryId = UUID.fromString(event.request().getParam("id"));

            UUID secret = UUID.fromString(data.getString("secret"));
            PasteEntry entry = accessor.byIdAndSecretKey(entryId, secret).one();
            dataMapper.fromJson(data, entry);
            Mapper<PasteEntry> dbMapper = manager.mapper(PasteEntry.class);
            dbMapper.save(entry);
            event.response().end(Json.encodePrettily(entry));
        }
    }
}
