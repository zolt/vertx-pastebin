package paste.bin.challenge.handlers;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.MappingManager;
import com.datastax.driver.mapping.Result;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.web.RoutingContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import paste.bin.challenge.entitiy.PasteEntry;
import paste.bin.challenge.entitiy.PasteEntryDao;

/**
 * Created with IntelliJ IDEA.<br/>
 * User: TimochkinEA<br/>
 * Date: 02.10.16<br/>
 * Time: 17:57<br/>
 * Handler for GET request
 */
@Component
public class GetEntriesHandler implements Handler<RoutingContext> {

    private Cluster cluster;

    @Autowired
    public GetEntriesHandler(Cluster cluster) {
        this.cluster = cluster;
    }

    @Override
    public void handle(RoutingContext event) {
        try(Session session = cluster.newSession()) {
            MappingManager manager = new MappingManager(session);
            PasteEntryDao accessor = manager.createAccessor(PasteEntryDao.class);
            Result<PasteEntry> result = accessor.publicEntries();
            JsonArray arrayNode = new JsonArray(result.all());
            event.response().end(arrayNode.encodePrettily());
        }
    }
}
