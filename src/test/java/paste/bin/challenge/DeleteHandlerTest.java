package paste.bin.challenge;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.<br/>
 * User: TimochkinEA<br/>
 * Date: 11.10.16<br/>
 * Time: 0:20<br/>
 * Test for {@link paste.bin.challenge.handlers.DeleteEntryHandler}
 */
public class DeleteHandlerTest extends BaseHandlersTest {

    private static final String TARGET_ID = "2cbb07c0-2ed4-4b49-bd99-15f9cd7e2ef2";

    private static final String TARGET_SECRET = "7256e473-4fcf-4e22-8f49-e28cb836db61";

    @Test
    @Override
    public void testHandler(TestContext context) {
        final Async async = context.async();
        final String json = new JsonObject().put("secret", TARGET_SECRET).encodePrettily();
        final String length = Integer.toString(json.length());

        vertx.createHttpClient().delete(port, "localhost", "/entries/" + TARGET_ID)
                .putHeader("content-type", "application/json")
                .putHeader("content-length", length)
                .handler(response -> response.bodyHandler(body -> {
                    JsonObject data = body.toJsonObject();
                    context.assertNotNull(data, "Response is empty");
                    context.assertEquals(data.getString("message"), "success");
                    async.complete();
                }))
                .write(json).end();
    }
}
