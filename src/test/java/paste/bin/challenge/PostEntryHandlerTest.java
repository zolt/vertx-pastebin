package paste.bin.challenge;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.apache.cassandra.cql3.Json;
import org.junit.Test;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.<br/>
 * User: TimochkinEA<br/>
 * Date: 10.10.16<br/>
 * Time: 22:49<br/>
 * Test for {@link paste.bin.challenge.handlers.PostEntryHandler}
 */
public class PostEntryHandlerTest extends BaseHandlersTest {

    @Test
    @Override
    public void testHandler(TestContext context) {
        final Async async = context.async();
        final JsonObject json = new JsonObject();
        json.put("title", "added entry title");
        json.put("body", "added entry body");
        json.put("private", false);

        final String postBody = json.encodePrettily();
        final String length = Integer.toString(postBody.length());

        vertx.eventBus().consumer("entry.created", handler -> {
            context.assertNotNull(handler.body());
            Map<String, String> data = (Map<String, String>) Json.decodeJson(handler.body().toString());
            context.assertEquals(data.get("title"), "added entry title");
            context.assertEquals(data.get("body"), "added entry body");
        });

        vertx.createHttpClient().post(port, "localhost", "/entries")
                .putHeader("content-type", "application/json")
                .putHeader("content-length", length)
                .handler(response -> response.bodyHandler(body -> {
                    JsonObject jsonResponse = body.toJsonObject();
                    context.assertNotNull(jsonResponse, "Response body is null");
                    context.assertTrue(jsonResponse.containsKey("secret"), "Secret key not found");
                    async.complete();
                }))
                .write(postBody).end();
    }
}
