package paste.bin.challenge;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.<br/>
 * User: TimochkinEA<br/>
 * Date: 10.10.16<br/>
 * Time: 23:39<br/>
 * Test for {@link paste.bin.challenge.handlers.PutEntryHandler}
 */
public class PutHandlerTest extends BaseHandlersTest {

    private static final String TARGET_ID = "e701c047-681c-4fe1-bf5b-7065b84a602b";

    private static final String TARGET_SECRET = "6ffcf11e-70f5-4262-afcf-ea5df860b454";

    private static final String TITLE = "Changed title";

    @Test
    @Override
    public void testHandler(TestContext context) {
        final Async async = context.async();
        final String json =
                new JsonObject()
                        .put("title", TITLE)
                        .put("body", "Private record body")
                        .put("private", true)
                        .put("secret", TARGET_SECRET).encodePrettily();
        final String length = Integer.toString(json.length());
        vertx.createHttpClient().put(port, "localhost", "/entries/" + TARGET_ID)
                .putHeader("content-type", "application/json")
                .putHeader("content-length", length)
                .handler(response -> response.bodyHandler(body -> {
                    JsonObject data = body.toJsonObject();
                    context.assertEquals(data.getString("id"), TARGET_ID, "Wrong entry id");
                    context.assertEquals(data.getString("title"), TITLE, "Title not equals");
                    context.assertEquals(data.getString("body"), "Private record body", "Entry body not equals");
                    context.assertTrue(data.getLong("added") > 0L, "Entry added date is empty");
                    context.assertEquals(data.getBoolean("private"), true,"Entry private property is empty");
                    context.assertEquals(data.getLong("expires"), Long.MAX_VALUE, "Entry expires not equals");
                    context.assertEquals(data.getString("secret"), TARGET_SECRET, "Secret key changed");
                    async.complete();
                }))
                .write(json).end();
    }
}
