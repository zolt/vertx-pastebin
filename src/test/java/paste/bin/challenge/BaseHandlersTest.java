package paste.bin.challenge;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.cassandraunit.CQLDataLoader;
import org.cassandraunit.dataset.cql.ClassPathCQLDataSet;
import org.cassandraunit.utils.EmbeddedCassandraServerHelper;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestContextManager;
import paste.bin.challenge.spring.AppConfig;
import paste.bin.challenge.verticles.WebServerVerticle;

/**
 * Created with IntelliJ IDEA.<br/>
 * User: TimochkinEA<br/>
 * Date: 10.10.16<br/>
 * Time: 22:50<br/>
 */
@RunWith(VertxUnitRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
@Ignore
public abstract class BaseHandlersTest implements ApplicationContextAware {

    Vertx vertx;

    Integer port = 8080;

    private ApplicationContext applicationContext;

    @Before
    public void setUp(TestContext context) throws Exception {
        EmbeddedCassandraServerHelper.startEmbeddedCassandra(600000L);
        System.setProperty("cluster.name", EmbeddedCassandraServerHelper.getClusterName());
        System.setProperty("cassandra.contact.point", EmbeddedCassandraServerHelper.getHost());
        System.setProperty("cassandra.contact.point.port", String.valueOf(EmbeddedCassandraServerHelper.getNativeTransportPort()));

        new TestContextManager(getClass()).prepareTestInstance(this);
        Cluster cluster = Cluster.builder().withClusterName(EmbeddedCassandraServerHelper.getClusterName())
                .withPort(EmbeddedCassandraServerHelper.getNativeTransportPort())
                .addContactPoint(EmbeddedCassandraServerHelper.getHost()).build();

        Session session = cluster.newSession();
        CQLDataLoader dataLoader = new CQLDataLoader(session);
        dataLoader.load(new ClassPathCQLDataSet("dataset.cql"));

        vertx = Vertx.vertx();
        DeploymentOptions options = new DeploymentOptions()
                .setConfig(new JsonObject().put("http.port", port)
                );
        vertx.deployVerticle(new WebServerVerticle(applicationContext), options, context.asyncAssertSuccess());
    }


    public abstract void testHandler(TestContext context);

    @After
    public void tearDown(TestContext context) {
        EmbeddedCassandraServerHelper.cleanEmbeddedCassandra();
        vertx.close(context.asyncAssertSuccess());
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }


}
