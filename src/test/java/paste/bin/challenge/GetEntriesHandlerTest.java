package paste.bin.challenge;

import io.vertx.core.json.JsonArray;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.<br/>
 * User: TimochkinEA<br/>
 * Date: 08.10.16<br/>
 * Time: 16:30<br/>
 * Test for {@link paste.bin.challenge.handlers.GetEntriesHandler}
 */
public class GetEntriesHandlerTest extends BaseHandlersTest {

    @Test
    @Override
    public void testHandler(TestContext context) {
        final Async async = context.async();
        vertx.createHttpClient().getNow(port, "localhost", "/entries", response -> response.bodyHandler(body -> {
            JsonArray array = body.toJsonArray();
            context.assertFalse(array.isEmpty(), "Body is empty!");
            context.assertEquals(3, array.getList().size(), "Wrong records count");
            async.complete();
        }));

    }
}
