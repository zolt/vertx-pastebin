#!/bin/bash
set -ex
bash /docker-entrypoint.sh -f &

sleep 60

exec "/usr/local/pastebin/bin/pastebin"
